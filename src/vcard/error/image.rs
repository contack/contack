use std::error::Error;
use std::fmt;

use vcard::values::image_value::ImageValueError;

#[derive(Debug)]
pub struct VCardImageValueError {
	side : ImageValueError
}

impl VCardImageValueError {

	/// Creates a Vcard Image Value Error from an Image Value Error
	pub fn from_image_value_error(
		err : ImageValueError
	) -> VCardImageValueError {
		VCardImageValueError{side : err}
	}
}

impl fmt::Display for VCardImageValueError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match &self.side {
			ImageValueError::FileMediaTypeCannotBeDefined => {
				writeln!(f, "File media type can be defined")
			},
			ImageValueError::MediaTypeNotImage => {
				writeln!(f, "Media Type Not Image")
			},
			ImageValueError::IOError(error) => {
				error.fmt(f)
			}
		}
	}
}

impl Error for VCardImageValueError {}

