
use std::error::Error;
use std::fmt;

use vcard::values::property_id_value::PropertyIDValueError;

#[derive(Debug)]
pub struct VCardPropertyIDValueError {
	side : PropertyIDValueError
}

impl VCardPropertyIDValueError {

	/// Creates a Vcard Property Id Value Error from an Property Id Value Error
	pub fn new (
		err : PropertyIDValueError
	) -> VCardPropertyIDValueError {
		VCardPropertyIDValueError {side : err}
	}
}

impl fmt::Display for VCardPropertyIDValueError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		writeln!(f, "Out of Range!")
	}
}

impl Error for VCardPropertyIDValueError {}

