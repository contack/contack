#[derive(Eq, PartialEq)]
pub enum ContactPlatform {
    Email,
    Tel,
    Discord,
    Matrix,
    Skype,
    Aim,
    Jabber,
    Icq,
    Groupwise,
    Gadugadu,
    Unknown,
}

impl ContactPlatform {
}

impl From<u8> for ContactPlatform {
    fn from(int: u8) -> Self {
        match int {
            0 => Self::Email,
            1 => Self::Tel,
            2 => Self::Discord,
            3 => Self::Matrix,
            4 => Self::Skype,
            5 => Self::Aim,
            6 => Self::Jabber,
            7 => Self::Icq,
            8 => Self::Groupwise,
            9 => Self::Gadugadu,
            _ => Self::Unknown,
        }
    }
}

impl From<ContactPlatform> for u8 {
    fn from(cp: ContactPlatform) -> Self {
        match cp {
            ContactPlatform::Email => 0,
            ContactPlatform::Tel => 1,
            ContactPlatform::Discord => 2,
            ContactPlatform::Matrix => 3,
            ContactPlatform::Skype => 4,
            ContactPlatform::Aim => 5,
            ContactPlatform::Jabber => 6,
            ContactPlatform::Icq => 7,
            ContactPlatform::Groupwise => 8,
            ContactPlatform::Gadugadu => 9,
            ContactPlatform::Unknown => 10,
        }
    }

}

impl std::fmt::Display for ContactPlatform {
    // Formats this
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ContactPlatform::Email => "Email",
                ContactPlatform::Tel => "Phone",
                ContactPlatform::Discord => "Discord",
                ContactPlatform::Matrix => "Matrix",
                ContactPlatform::Skype => "Skype",
                ContactPlatform::Aim => "Aim",
                ContactPlatform::Jabber => "Jabber",
                ContactPlatform::Icq => "ICQ",
                ContactPlatform::Groupwise => "GroupWise",
                ContactPlatform::Gadugadu => "GaduGadu",
                ContactPlatform::Unknown => "Unkown",
            }
        )
    }
}
