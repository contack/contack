//! # Address
//!
//! This modules contains structures to do with addresses, positions and
//! suchlike.

#[derive(PartialEq)]
#[non_exhaustive]
/// A simple structure to hold address information
pub struct Address {

    /// This is the street where the address points to.
    pub street: Option<String>,

    /// This is the locality/city where the address points to.
    pub locality: Option<String>,

    /// This is the region/county where the address points to.
    pub region: Option<String>,

    /// This is the code where the address is. This may be a zip/postal code.
    pub code: Option<String>,

    /// This is the country where ths address points to.
    pub country: Option<String>,

    /// This is the longitude and latitude of the address.
    pub geo: Option<Geo>,
}

#[derive(PartialOrd, PartialEq)]
#[non_exhaustive]
/// A simple structure to hold Geo location.
pub struct Geo {
    /// This is the longitude for the geo-location.
    pub longitude: f64,

    /// This is the latitude for the geo-location.
    pub latitude: f64,
}

impl Address {
    /// Creates a new address
    #[must_use]
    pub const fn new(
        street: Option<String>,
        locality: Option<String>,
        region: Option<String>,
        code: Option<String>,
        country: Option<String>,
    ) -> Self {
        Self {
            street,
            locality,
            region,
            code,
            country,
            geo: None,
        }
    }
}

impl Geo {
    #[must_use]
    pub const fn new(longitude: f64, latitude: f64) -> Self {
        Self {
            longitude,
            latitude,
        }
    }
}
