#![warn(
    clippy::all,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo,
)]
 
pub mod address;
pub mod contact_information;
pub mod contact_platform;
pub mod date_time;
pub mod name;
pub mod org;
pub mod uri;
#[cfg(feature = "to_vcard")]
pub mod vcard;

#[allow(dead_code)] // This struct is dead 😵
/// This is the base structure to hold all contact details
pub struct Contact {
    /*
     * Identification Property
     */
    /// The Contacts Name
    pub name: name::Name,

    /// The Contact's Nickname
    pub nickname: Option<String>,

    /// The Contact's Anniversary
    pub anniversary: Option<date_time::DateTime>,

    /// The Contact's Birthday
    pub bday: Option<date_time::DateTime>,

    /// The Contact's Photo
    pub photo: Option<uri::Uri>,

    /*
     * Organisational Properties
     */
    /// The Contact's Job Title
    pub title: Option<String>,

    /// The Contact's Job Role
    pub role: Option<String>,

    /// The Contact's Organization
    pub org: Option<org::Org>,

    /// The Contact's Organization's Logo
    pub logo: Option<uri::Uri>,

    /*
     * Communication Properties
     */
    /// The Contact's contact information
    pub contact_information: std::vec::Vec<contact_information::ContactInformation>,

    /*
     * Deliver Addressing Properties
     */
    /// The Contact's Work Address
    pub work_address: Option<address::Address>,

    /// The Contact's Home Address
    pub home_address: Option<address::Address>,

    /*
     * Explanatory Properties
     */
    /// The Contact's Uid
    pub uid: String,
}

impl Contact {
    pub fn gen_uid(&mut self) {
        // Generates a UID from the UUID Value
        self.uid = uuid::Uuid::new_v4().to_string();
    }

    #[must_use]
    pub fn new(name: name::Name) -> Self {
        Self {
            name,
            nickname: None,
            anniversary: None,
            bday: None,
            photo: None,

            title: None,
            role: None,
            org: None,
            logo: None,

            contact_information: std::vec::Vec::new(),

            work_address: None,
            home_address: None,

            uid: uuid::Uuid::new_v4().to_string(),
        }
    }
}

#[cfg(feature = "to_vcard")]
impl std::convert::TryInto<vcard::vcard::VCard> for Contact {
    type Error = Box<dyn std::error::Error>;
    
    fn try_into(self) -> Result<vcard::vcard::VCard, Self::Error> {
        vcard::contack_to_vcard(&self)
    }
}
