use std::vec::Vec;

/// A struct which can either store byte information or a url
#[derive(Eq, PartialEq, Debug)]
pub enum Uri {
    Url { url: String },
    Bytes { val: Vec<u8>, mime: String },
}

impl Uri {
    /// Creates a new url from a string
    #[must_use]
    pub const fn new_url(string: String) -> Self {
        Self::Url { url: string }
    }

    /// Creates a new bytes from a vec
    #[must_use]
    pub const fn new_bytes(bytes: Vec<u8>, mime: String) -> Self {
        Self::Bytes {
            val: bytes,
            mime,
        }
    }

    /// Creates a new bytes from a &[u8]
    #[must_use]
    pub fn new_bytes_array(bytes: &[u8], mime: String) -> Self {
        Self::Bytes {
            val: bytes.to_vec(),
            mime,
        }
    }
}
