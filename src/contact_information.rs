use crate::contact_platform::ContactPlatform;

pub struct ContactInformation {
    /// The Preference Value
    pub pref: u8,

    /// The Value (for example johndoe@example.com)
    pub value: String,

    // This pids, used for representing this
    pub pid: String,

    /// The platform which this is on
    pub platform: ContactPlatform,

    /// The type
    pub typ: Option<Type>,
}

#[derive(Eq, PartialEq)]
pub enum Type {
    Work,
    Home,
}

impl ContactInformation {
    /// Creates a new Contact Information
    #[must_use]
    pub fn new(string: String, platform: ContactPlatform) -> Self {
        Self {
            pref: 0,
            value: string,
            pid: uuid::Uuid::new_v4().to_string(),
            platform,
            typ: None,
        }
    }
}
