#[derive(Eq, PartialEq, Hash)]
pub struct Org {
    pub org: String,
    pub unit: String,
    pub office: String,
}

impl Org {
    /// Creates a new Org
    #[must_use]
    pub const fn new(org: String, unit: String, office: String) -> Self {
        Self {
            org,
            unit,
            office,
        }
    }
}
