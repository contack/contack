use std::fmt;
use std::fmt::Display;

/// Simple struct to store a name
pub struct Name {
    pub given: Option<String>,
    pub additional: Option<String>,
    pub family: Option<String>,
    pub prefixes: Option<String>,
    pub suffixes: Option<String>,
}

impl Name {
    /// Creates a new Name
    #[must_use]
    pub const fn new(
        given: Option<String>,
        additional: Option<String>,
        family: Option<String>,
        prefixes: Option<String>,
        suffixes: Option<String>,
    ) -> Self {
        Self {
            given,
            additional,
            family,
            prefixes,
            suffixes,
        }
    }
}

// For .to_string()
impl Display for Name {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut string = "".to_string();

        if self.prefixes != None {
            string.push(' ');
            string.push_str(self.prefixes.as_ref().unwrap());
            string = string.trim().to_string();
        }

        if self.given != None {
            string.push(' ');
            string.push_str(self.given.as_ref().unwrap());
            string = string.trim().to_string();
        }

        if self.additional != None {
            string.push(' ');
            string.push_str(self.additional.as_ref().unwrap());
            string = string.trim().to_string();
        }

        if self.family != None {
            string.push(' ');
            string.push_str(self.family.as_ref().unwrap());
            string = string.trim().to_string();
        }

        if self.suffixes != None {
            string.push(' ');
            string.push_str(self.suffixes.as_ref().unwrap());
            string = string.trim().to_string();
        }

        write!(f, "{}", string)
    }
}
